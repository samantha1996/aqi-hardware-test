#define SENSOR_PIN 9 

#define GPS_RX 6
#define GPS_TX 7

#include <SoftwareSerial.h>

SoftwareSerial gpsSensor(GPS_RX, GPS_TX); 

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200); 
  gpsSensor.begin(9600);
  Serial.println("Welcome to home AQI station!");
  Serial.println("============================"); 
  Serial.println("GPS Module takes around 15 minutes to detect a satellite"); 

  float wait_time = 0; 
  Serial.print(":");
  while(wait_time < 15){
    Serial.print("="); 
    delay(500); 
    wait_time += 0.075; 
  }
  
    Serial.println(":"); 
}


void loop() {
  // put your main code here, to run repeatedly:
  int aqi = analogRead(SENSOR_PIN); 
  
  Serial.print("Air Quality Index: ");
  Serial.print(aqi, DEC); 
  Serial.print(" PPM");

  if(aqi < 100){
    Serial.println(", Fresh Air");
    } else if(aqi > 100 && aqi <= 250){
      Serial.println(", Clean Air");
    } else if(aqi > 250 && aqi <= 500){
      Serial.println(", Getting Poor"); 
    } else if(aqi > 500 && aqi <= 750){
      Serial.println(", Poor"); 
    } else if(aqi > 750 && aqi <= 1000){
      Serial.println(", Very Poor");
    } else if(aqi > 1000) {
      Serial.println(", Evacuation is recommended");
    }

  if(gpsSensor.available() <= 0){
    Serial.println("Location not detected yet"); 
    } else {
    Serial.print("Location: "); 
    Serial.write(gpsSensor.read()); 
    }
    // This part is just for debugging the shit out of this
    /*
    Serial.println(" -> "); 
    Serial.print("Debugging mode: "); 
    Serial.println(gpsSensor.read());
   //delay(3000); 
  */
}
